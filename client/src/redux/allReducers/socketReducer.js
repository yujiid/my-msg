export const socketReducer = (state = '', action) => {
  switch (action.type) {
    case 'CREATE_SOCKET':
      return action.payload;
    default:
      return state;
  }
};
