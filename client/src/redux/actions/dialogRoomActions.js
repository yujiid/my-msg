export const createSocket = payload => {
  return {
    type: 'CREATE_SOCKET',
    payload
  };
};
