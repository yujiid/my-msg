import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import ScrollToBottom from 'react-scroll-to-bottom';
import styles from './messages.module.scss';

import Message from './Message';

const sn = classNames(styles);

export const Messages = ({ messages, name }) => (
  <ScrollToBottom className='messages'>
    {messages.map((message, i) => (
      <div key={i}>
        <Message message={message} name={name} />
      </div>
    ))}
  </ScrollToBottom>
);
