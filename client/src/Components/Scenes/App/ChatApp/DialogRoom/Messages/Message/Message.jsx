import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './message.module.scss';

const sn = classNames(styles);

import ReactEmoji from 'react-emoji';

export const Message = ({ message: { text, user }, name }) => {
  let isSentByCurrentUser = false;

  const trimmedName = name.trim().toLowerCase();

  if (user === trimmedName) {
    isSentByCurrentUser = true;
  }

  return isSentByCurrentUser ? (
    <div className='message message_justify-end'>
      <p className='message__user-name user-name_padding-right-10'>
        {trimmedName}
      </p>
      <div className='message__message-box message-box message-box_background-orange'>
        <p className='message-text message-text_color-white'>
          {ReactEmoji.emojify(text)}
        </p>
      </div>
    </div>
  ) : (
    <div className='message message_justify-start'>
      <div className='message__message-box message-box message-box_background-light'>
        <p className='message-text message-text_color-dark'>
          {ReactEmoji.emojify(text)}
        </p>
      </div>
      <p className='message__user-name user-name_padding-left-10'>{user}</p>
    </div>
  );
};
