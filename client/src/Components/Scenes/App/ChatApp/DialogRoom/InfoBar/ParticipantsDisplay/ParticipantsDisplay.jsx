import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './participantsDisplay.module.scss';

const sn = classNames(styles);

export const ParticipantsDisplay = ({ users }) => (
  <div className={sn('participants-display')}>
    {users ? (
      <div>
        <span>People currently chatting: {users.length}</span>
      </div>
    ) : null}
  </div>
);
