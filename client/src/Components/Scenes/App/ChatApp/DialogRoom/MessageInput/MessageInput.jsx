import React, { useState, useEffect } from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './MessageInput.module.scss';

const sn = classNames(styles);

export const MessageInput = ({ message, setMessage, sendMessage }) => {
  return (
    <div className={sn('message-input')}>
      <form className={sn('form-input')}>
        <button className={sn('form-input__attach-btn attach-btn')}></button>
        <input
          className={sn('form-input__input input')}
          placeholder='Type a message...'
          type='text'
          value={message}
          onChange={e => setMessage(e.target.value)}
          onKeyPress={e => (e.key === 'Enter' ? sendMessage(e) : null)}
        />
        <button
          onClick={e => sendMessage(e)}
          className={sn('form-input__send-button send-button')}
        >
          Send
        </button>
      </form>
    </div>
  );
};
