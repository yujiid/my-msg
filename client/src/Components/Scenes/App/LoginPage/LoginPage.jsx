import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { classNames } from 'utilities/classNameUtility';
import styles from './loginPage.module.scss';

import { signIn } from 'redux/actions/loginPageActions';

const sn = classNames(styles);

export const LoginPage = () => {
  const [userName, setUserName] = useState('');
  const [room, setRoom] = useState('');
  const dispatch = useDispatch();

  return (
    <div className={sn('login-page')}>
      <div className={sn('login-page__container container')}>
        <h1 className={sn('container__header')}>JOIN</h1>
        <div className={sn('container__input-container input-container')}>
          <div className='input-container__label input-container__label_user-name'>
            <input
              className={sn('input-container__input')}
              placeholder='Name'
              type='text'
              id='userName'
              onChange={e => setUserName(e.target.value)}
            />
          </div>
          <div className='input-container__label input-container__label_room-name'>
            <input
              className={sn('input-container__input')}
              placeholder='Room'
              type='text'
              id='roomName'
              onChange={e => {
                setRoom(e.target.value);
              }}
            />
          </div>
        </div>
        <div
          className={sn(
            'container__join-button-container join-button-container'
          )}
        >
          <Link
            to={`/chat-app?userName=${userName}&room=${room}`}
            onClick={e =>
              !userName || !room
                ? e.preventDefault
                : dispatch(signIn({ userName, room }))
            }
          >
            <button
              className={sn(
                'join-button-container__button button button__margin-top-20'
              )}
            >
              Join
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};
