//const path = require('path');
module.exports = {
  /**
   * This is the main entry point for your application, it's the first file
   * that runs in the main process.
   */
  entry: './src/main.js',
  // Put your normal webpack config below here
  /*
  resolve: {
    modules: [path.resolve(__dirname, './src'), 'node_modules'],
    extensions: ['.js'],
    alias: { reducers: path.resolve(__dirname, './src/utilities') }
  },*/
  module: {
    rules: require('./webpack.rules')
  },
  resolve: require('./webpack.resolves')
};
